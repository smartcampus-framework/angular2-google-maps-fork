import {Injectable, NgZone, EventEmitter} from 'angular2/core';
import {Observer} from 'rxjs/Observer';
import {Observable} from 'rxjs/Observable';
import {SebmGoogleMapMarker} from '../directives/google-map-marker';
import {GoogleMapsAPIWrapper} from './google-maps-api-wrapper';
import {Marker, OverlayView} from './google-maps-types';
import {GoogleMap} from "./google-maps-types";

@Injectable()
export class MarkerManager {
  private _markers: Map<SebmGoogleMapMarker, Promise<Marker>> =
    new Map<SebmGoogleMapMarker, Promise<Marker>>();

  constructor(private _mapsWrapper: GoogleMapsAPIWrapper, private _zone: NgZone) {
  }

  deleteMarker(marker: SebmGoogleMapMarker): Promise<void> {
    const m = this._markers.get(marker);
    if (m == null) {
      // marker already deleted
      return Promise.resolve();
    }
    return m.then((m: Marker) => {
      return this._zone.run(() => {
        m.setMap(null);
        this._markers.delete(marker);
      });
    });
  }

  updateMarkerPosition(marker: SebmGoogleMapMarker): Promise<void> {
    return this._markers.get(marker).then(
      (m: Marker) => m.setPosition({lat: marker.latitude, lng: marker.longitude}));
  }

  updateTitle(marker: SebmGoogleMapMarker): Promise<void> {
    return this._markers.get(marker).then((m: Marker) => m.setTitle(marker.title));
  }

  updateLabel(marker: SebmGoogleMapMarker): Promise<void> {
    return this._markers.get(marker).then((m: Marker) => {
      m.setLabel(marker.label);
    });
  }

  updateDraggable(marker: SebmGoogleMapMarker): Promise<void> {
    return this._markers.get(marker).then((m: Marker) => m.setDraggable(marker.draggable));
  }

  updateIcon(marker: SebmGoogleMapMarker): Promise<void> {
    return this._markers.get(marker).then((m: Marker) => m.setIcon(marker.iconUrl));
  }

  addMarker(marker: SebmGoogleMapMarker) {
    const markerPromise = this._mapsWrapper.createMarker({
      position: {lat: marker.latitude, lng: marker.longitude},
      label: marker.label,
      draggable: marker.draggable,
      icon: marker.iconUrl
    });
    this._markers.set(marker, markerPromise);
  }

  getNativeMarker(marker: SebmGoogleMapMarker): Promise<Marker> {
    return this._markers.get(marker);
  }

  createEventObservable<T>(eventName: string, marker: SebmGoogleMapMarker): Observable<T> {
    return Observable.create((observer: Observer<T>) => {
      this._markers.get(marker).then((m: Marker) => {
        m.addListener(eventName, (e: T) => this._zone.run(() => observer.next(e)));
      });
    });
  }

  addTextLabel(marker: SebmGoogleMapMarker, text: string, markerClick: EventEmitter<void>) {
    this._mapsWrapper.getMap().then((map: GoogleMap) => {
      new CustomOverlayView(map, marker, markerClick);
    });
  }
}

class CustomOverlayView {

  constructor(map: GoogleMap, marker: SebmGoogleMapMarker, markerClick: EventEmitter<void>) {
    let overlayView: OverlayView = GoogleMapsAPIWrapper.createOverlayView();

    overlayView.div_ = null;
    overlayView.map_ = map;

    overlayView.onAdd = () => {
      // Note: an overlay's receipt of onAdd() indicates that
      // the map's panes are now available for attaching
      // the overlay to the map via the DOM.

      // Create the DIV and set some basic attributes.
      var div = document.createElement('DIV');
      // div.className = this.cls_;

      div.innerHTML = marker.textLabel.text;

      // Set the overlay's div_ property to this DIV
      overlayView.div_ = div;
      var overlayProjection = overlayView.getProjection();

      var panes = overlayView.getPanes();
      var position = overlayProjection.fromLatLngToDivPixel(marker.latLng);
      div.className = marker.textLabel.className;
      div.style.position = 'absolute';
      div.style.left = position.x - div.offsetWidth / 2 + 'px';
      div.style.top = position.y + 'px';

      div.addEventListener('click', ()=>markerClick.next(null));

      panes.floatPane.appendChild(div);
    };

    overlayView.draw = () => {
      var overlayProjection = overlayView.getProjection();

      // Retrieve the southwest and northeast coordinates of this overlay
      // in latlngs and convert them to pixels coordinates.
      // We'll use these coordinates to resize the DIV.
      var div = overlayView.div_;
      var position = overlayProjection.fromLatLngToDivPixel(marker.latLng);
      div.style.left = position.x - div.offsetWidth / 2 + 'px';
      div.style.top = position.y + 'px';
    };

    overlayView.setMap(map);
  }
}
