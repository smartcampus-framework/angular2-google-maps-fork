/**
 * angular2-google-maps - Angular 2 components for Google Maps
 * @version v0.9.0
 * @link https://github.com/SebastianM/angular2-google-maps#readme
 * @license MIT
 */
export * from './directives';
export * from './services';
export * from './events';
export declare const ANGULAR2_GOOGLE_MAPS_PROVIDERS: any[];
