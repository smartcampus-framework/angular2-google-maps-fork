/**
 * angular2-google-maps - Angular 2 components for Google Maps
 * @version v0.9.0
 * @link https://github.com/SebastianM/angular2-google-maps#readme
 * @license MIT
 */
import { SebmGoogleMap } from './directives/google-map';
import { SebmGoogleMapMarker } from './directives/google-map-marker';
import { SebmGoogleMapInfoWindow } from './directives/google-map-info-window';
export const ANGULAR2_GOOGLE_MAPS_DIRECTIVES = [SebmGoogleMap, SebmGoogleMapMarker, SebmGoogleMapInfoWindow];

//# sourceMappingURL=directives-const.js.map
