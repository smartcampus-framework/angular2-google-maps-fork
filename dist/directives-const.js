/**
 * angular2-google-maps - Angular 2 components for Google Maps
 * @version v0.9.0
 * @link https://github.com/SebastianM/angular2-google-maps#readme
 * @license MIT
 */
"use strict";
var google_map_1 = require('./directives/google-map');
var google_map_marker_1 = require('./directives/google-map-marker');
var google_map_info_window_1 = require('./directives/google-map-info-window');
exports.ANGULAR2_GOOGLE_MAPS_DIRECTIVES = [google_map_1.SebmGoogleMap, google_map_marker_1.SebmGoogleMapMarker, google_map_info_window_1.SebmGoogleMapInfoWindow];

//# sourceMappingURL=directives-const.js.map
