/**
 * angular2-google-maps - Angular 2 components for Google Maps
 * @version v0.9.0
 * @link https://github.com/SebastianM/angular2-google-maps#readme
 * @license MIT
 */
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('angular2/core');
var Observable_1 = require('rxjs/Observable');
var google_maps_api_wrapper_1 = require('./google-maps-api-wrapper');
var MarkerManager = (function () {
    function MarkerManager(_mapsWrapper, _zone) {
        this._mapsWrapper = _mapsWrapper;
        this._zone = _zone;
        this._markers = new Map();
    }
    MarkerManager.prototype.deleteMarker = function (marker) {
        var _this = this;
        var m = this._markers.get(marker);
        if (m == null) {
            // marker already deleted
            return Promise.resolve();
        }
        return m.then(function (m) {
            return _this._zone.run(function () {
                m.setMap(null);
                _this._markers.delete(marker);
            });
        });
    };
    MarkerManager.prototype.updateMarkerPosition = function (marker) {
        return this._markers.get(marker).then(function (m) { return m.setPosition({ lat: marker.latitude, lng: marker.longitude }); });
    };
    MarkerManager.prototype.updateTitle = function (marker) {
        return this._markers.get(marker).then(function (m) { return m.setTitle(marker.title); });
    };
    MarkerManager.prototype.updateLabel = function (marker) {
        return this._markers.get(marker).then(function (m) {
            m.setLabel(marker.label);
        });
    };
    MarkerManager.prototype.updateDraggable = function (marker) {
        return this._markers.get(marker).then(function (m) { return m.setDraggable(marker.draggable); });
    };
    MarkerManager.prototype.updateIcon = function (marker) {
        return this._markers.get(marker).then(function (m) { return m.setIcon(marker.iconUrl); });
    };
    MarkerManager.prototype.addMarker = function (marker) {
        var markerPromise = this._mapsWrapper.createMarker({
            position: { lat: marker.latitude, lng: marker.longitude },
            label: marker.label,
            draggable: marker.draggable,
            icon: marker.iconUrl
        });
        this._markers.set(marker, markerPromise);
    };
    MarkerManager.prototype.getNativeMarker = function (marker) {
        return this._markers.get(marker);
    };
    MarkerManager.prototype.createEventObservable = function (eventName, marker) {
        var _this = this;
        return Observable_1.Observable.create(function (observer) {
            _this._markers.get(marker).then(function (m) {
                m.addListener(eventName, function (e) { return _this._zone.run(function () { return observer.next(e); }); });
            });
        });
    };
    MarkerManager.prototype.addTextLabel = function (marker, text, markerClick) {
        this._mapsWrapper.getMap().then(function (map) {
            new CustomOverlayView(map, marker, markerClick);
        });
    };
    MarkerManager = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [google_maps_api_wrapper_1.GoogleMapsAPIWrapper, core_1.NgZone])
    ], MarkerManager);
    return MarkerManager;
}());
exports.MarkerManager = MarkerManager;
var CustomOverlayView = (function () {
    function CustomOverlayView(map, marker, markerClick) {
        var overlayView = google_maps_api_wrapper_1.GoogleMapsAPIWrapper.createOverlayView();
        overlayView.div_ = null;
        overlayView.map_ = map;
        overlayView.onAdd = function () {
            // Note: an overlay's receipt of onAdd() indicates that
            // the map's panes are now available for attaching
            // the overlay to the map via the DOM.
            // Create the DIV and set some basic attributes.
            var div = document.createElement('DIV');
            // div.className = this.cls_;
            div.innerHTML = marker.textLabel.text;
            // Set the overlay's div_ property to this DIV
            overlayView.div_ = div;
            var overlayProjection = overlayView.getProjection();
            var panes = overlayView.getPanes();
            var position = overlayProjection.fromLatLngToDivPixel(marker.latLng);
            div.className = marker.textLabel.className;
            div.style.position = 'absolute';
            div.style.left = position.x - div.offsetWidth / 2 + 'px';
            div.style.top = position.y + 'px';
            div.addEventListener('click', function () { return markerClick.next(null); });
            panes.floatPane.appendChild(div);
        };
        overlayView.draw = function () {
            var overlayProjection = overlayView.getProjection();
            // Retrieve the southwest and northeast coordinates of this overlay
            // in latlngs and convert them to pixels coordinates.
            // We'll use these coordinates to resize the DIV.
            var div = overlayView.div_;
            var position = overlayProjection.fromLatLngToDivPixel(marker.latLng);
            div.style.left = position.x - div.offsetWidth / 2 + 'px';
            div.style.top = position.y + 'px';
        };
        overlayView.setMap(map);
    }
    return CustomOverlayView;
}());

//# sourceMappingURL=marker-manager.js.map
